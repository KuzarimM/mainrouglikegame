#pragma once

#include "stdafx.h"
#include "RougLikeGame.h"
#include <stdio.h>
#include <ctime>
#include "MapGenerator.h"
#include"PIHelper.h"



void NullIt(HDC hdc, int x, int y, int CellSize, HBRUSH hBrush);

struct Item
{
	int ID = NULL_ITEM;
	void (*DrowPicures)(HDC, int, int, int,HBRUSH) = NullIt;
	TCHAR name[40];
	TCHAR info[15][29];
	HBRUSH MyBrush = CreateSolidBrush(RGB(96, 96, 96));
	int count = 0;
	void (*FirstUse)(void*, void*);
	void (*SecondUse)(void*, void*,void*);
	bool IsWeapon = false;
	int Damage = 1;
	bool IsArmor = false;
	int Armor = 0;
	bool IsRing = false;
	int RingType = -1;

};


struct Block
{
	int ID;
	void (*DrowPicures)(HDC, int, int, int, HBRUSH, int par);
	HBRUSH MyBrush = CreateSolidBrush(RGB(96, 96, 96));
};

struct Effect
{
	int ID = NULL_EFFECT;
	int Time=0;
	TCHAR name[29];
	TCHAR Info[15][29];
	void(*Action)(void *, int, int,int);
	int pover = -1;
};

extern Item AllItems[];

extern Block AllBlocks[];

extern Effect ALLEffects[];

extern Item UnknownPoushen;

