#include "stdafx.h"
#include "Enemys.h"
#include "GameContriller.h"
#include <math.h>;

void Enemy::GetDamage(int damage) {
	HP -= damage;
	HP = min(HP, maxHp);
	if (HP <= 0) {
		Die = 2;
		Map[y][x] = LastBlock;
	}
}

double Enemy::GetProcent() {
	return (HP*1.0 / maxHp);
}

void Enemy::AddEffect(Effect eff, int time, int pover) {
	bool flag = false;
	for (int i = 0; i < EFFECT_BAR_SIZE; i++)
	{
		if (EffectBar[i].ID == eff.ID) {
			EffectBar[i].Time = max(EffectBar[i].Time, eff.Time);
			flag = true;
			break;
		}
	}
	if (!flag) {
		for (int i = 0; i < EFFECT_BAR_SIZE; i++)
		{
			if (EffectBar[i].ID == NULL_EFFECT) {
				EffectBar[i] = eff;
				if (pover != -1) {
					EffectBar[i].pover = pover;
				}
				if (time != -1) {
					EffectBar[i].Time = time;
				}
				break;
			}
		}
	}
}

bool Enemy::RemoveEffect(int id) {
	for (int i = 0; i < EFFECT_BAR_SIZE; i++)
	{
		if (EffectBar[i].ID == id) {
			EffectBar[i] = ALLEffects[0];
			return true;
		}
	}
	return false;
}




int EnemyCoord[(MAP_LENTH / CELL_SIZE) * (MAP_LENTH / CELL_SIZE)][2];
int EnemyCount = 0;

Enemy AllEnemys[(MAP_LENTH / CELL_SIZE) * (MAP_LENTH / CELL_SIZE)];


void CreateAllEnemys() {
	int tmpCount = 0;
	for (int i = 0; i < EnemyCount; i++)
	{
		int x = EnemyCoord[i][0];
		int y = EnemyCoord[i][1];
		if (Map[y][x] != 1) {
			Enemy* tmp = new Enemy;
			tmp->x = x;
			tmp->y = y;
			tmp->Type = (GetRandomInt(0,10)>=7)?1:0;
			tmp->HP = GetRandomInt(2, tmp->maxHp);
			tmp->maxHp = tmp->HP;
			AllEnemys[tmpCount] = *tmp;
			tmpCount++;
		}
	}
	EnemyCount = tmpCount;
}


int CheckSeeing(int x, int y, int ex, int ey) {
	
	if (abs(ex - x)*abs(ey - y) <= 1) {
		return (int)sqrt((ex - x)*(ex - x) + (ey - y)*(ey - y));
	}
	if (ex == x) {
		for (int i = y; (y <= ey) ? (i < ey) : (i > ey); i += (y <= ey) ? 1 : -1)
		{
			if (Map[i][x] == WALL_ID || Map[i][x] == DOOR_ID || Map[i][x] == KEY_DOR_ID) {
				return false;
			}
		}
		return true;
	}

	double k = (double)(ey - y) / (ex - x);
	double b = y - k * x;
	for (int i = x; (x <= ex) ? (i < ex) : (i > ex); i += (x <= ex) ? 1 : -1)
	{
		double tmpy = k * i + b;
		if (Map[(int)tmpy][i] == WALL_ID|| Map[(int)tmpy][i] == DOOR_ID|| Map[(int)tmpy][i] == KEY_DOR_ID) {
			return false;
		}
	}
	for (int i = y; (y <= ey) ? (i < ey) : (i > ey); i += (y <= ey) ? 1 : -1)
	{
		double tmpx = (i - b) / k;
		if (Map[i][(int)tmpx] == WALL_ID|| Map[i][(int)tmpx] == DOOR_ID|| Map[i][(int)tmpx]==KEY_DOR_ID) {
			return false;
		}
	}
	return (int)sqrt((ex-x)*(ex-x)+(ey-y)*(ey-y));
	
}

void MoveAllEnemy(void* Pl) {
	Player* P = static_cast<Player*>(Pl);
	int Ply = P->PlayerX;
	int Plx = P->PlayerY;
	for (int k = 0; k < EnemyCount; k++)
	{
		if (AllEnemys[k].Die==2) {
			continue;
		}
		int x = AllEnemys[k].x;
		int y = AllEnemys[k].y;
		for (int i = 0; i < 50; i++)
		{
			if (AllEnemys[k].EffectBar[i].ID != NULL_EFFECT) {
				AllEnemys[k].EffectBar[i].Action(&AllEnemys[k], AllEnemys[k].EffectBar[i].Time, -1, 1);
			}
		}
		int ch = CheckSeeing(x, y, Plx, Ply);
		if (((bool)ch != (AllEnemys[k].Type == 2))) {
			AllEnemys[k].Type = ch ? 2 : GetRandomInt(0, 1);
			if (AllEnemys[k].Type==0) {
				Map[y][x] = ENEMY_ID;
			}
			AllEnemys[k].OnPath = -1;

		}
		if ((AllEnemys[k].OnPath == -1&& AllEnemys[k].Type!=0)||AllEnemys[k].Type == 2) {
			AllEnemys[k].OnPath = 0;
			int Mask[MAP_LENTH][MAP_LENTH] = {};
			int Och[MAP_LENTH * MAP_LENTH * 4][2] = {};
			int Up = 0;
			int Down = 0;
			Mask[y][x] = 1;
			Och[Up][0] = x;
			Och[Up][1] = y;
			Up++;
			while (Up > Down)
			{
				int nx = Och[Down][0];
				int ny = Och[Down][1];
				int count = Mask[ny][nx] + 1;
				Down++;
				for (int i = max(0, nx - 1); i <= min(MAP_LENTH - 1, nx + 1); i++)
				{
					for (int j = max(0, ny - 1); j <= min(MAP_LENTH - 1, ny + 1); j++)
					{
						if ((i == nx && j != ny) || (i != nx && j == ny)) {
							if (Map[j][i] != WALL_ID && Map[j][i] != KEY_DOR_ID) {
								if (Mask[j][i] == 0 || Mask[j][i] > count) {
									Och[Up][0] = i;
									Och[Up][1] = j;
									Mask[j][i] = count;
									Up++;
								}
							}
						}
					}
				}
			}
			
			int Targx = 0;
			int Targy = 0;
			if (AllEnemys[k].Type == 2 && Mask[Ply][Plx]>1) {
				Targx = Plx;
				Targy = Ply;
				
			}
			else {
				do
				{
					Targx = GetRandomInt(max(0, x - 30), min(x + 30, MAP_LENTH - 1));
					Targy = GetRandomInt(max(0, y - 30), min(y + 30, MAP_LENTH - 1));
				} while (Mask[Targy][Targx] <= 1);
			}
			while (Mask[Targy][Targx] != 1) {
				AllEnemys[k].Path[AllEnemys[k].OnPath][0] = Targx;
				AllEnemys[k].Path[AllEnemys[k].OnPath][1] = Targy;
				AllEnemys[k].OnPath++;
				int tmpx = -1;
				int tmpy = -1;
				int MinV = Mask[Targy][Targx];
				for (int i = max(0,Targy-1); i <= min(MAP_LENTH-1,Targy+1); i++)
				{
					for (int j = max(0, Targx - 1); j <= min(MAP_LENTH - 1, Targx + 1); j++)
					{
						if ((i == Targy && j != Targx) || (i != Targy && j == Targx)) {
							if (Mask[i][j] != 0 && Mask[i][j] < MinV) {
								MinV = Mask[i][j];
								tmpy = i;
								tmpx = j;
							}
						}
					}
				}
				Targx = tmpx;
				Targy = tmpy;
			}
			AllEnemys[k].OnPath--;
		}
		int r = GetRandomInt(0, (ch-2)*2);
		if (AllEnemys[k].Type ==1 ||(AllEnemys[k].Type == 2 && r<=(ch*1.5))) {
			if (AllEnemys[k].Path[AllEnemys[k].OnPath][0] == Plx && AllEnemys[k].Path[AllEnemys[k].OnPath][1] == Ply) {
				P->GetDamage(GetRandomInt(0,AllEnemys[k].Damage), true);
			}
			else if(Map[AllEnemys[k].Path[AllEnemys[k].OnPath][1]][AllEnemys[k].Path[AllEnemys[k].OnPath][0]]!=ENEMY_ID) {
				Map[y][x] = AllEnemys[k].LastBlock;
				x = AllEnemys[k].Path[AllEnemys[k].OnPath][0];
				y = AllEnemys[k].Path[AllEnemys[k].OnPath][1];
				AllEnemys[k].OnPath--;
				AllEnemys[k].LastBlock = Map[y][x];
				if (AllEnemys[k].LastBlock == PLAYER_ID || AllEnemys[k].LastBlock == ENEMY_ID) {
					AllEnemys[k].LastBlock = 0;
				}
				Map[y][x] =  ENEMY_ID;
				AllEnemys[k].x = x;
				AllEnemys[k].y = y;
			}
		}
		if (AllEnemys[k].Die == 1) {
			AllEnemys[k].Die = 2;
		}
	}
	int a = 0;
	a++;
}

Enemy * GetEnemyFromCoord(int x, int y) {
	for (int i = 0; i < EnemyCount; i++)
	{
		if (AllEnemys[i].x == x && AllEnemys[i].y == y&& AllEnemys[i].Die!=2) {
			return &AllEnemys[i];
		}
	}
	return NULL;
}
