
#include "stdafx.h"
#include "RougLikeGame.h"
#include <stdio.h>
#include <ctime>
#include "GameContriller.h"
#include "MapGenerator.h"
#include"PIHelper.h"
#include "Entities.h"
#include "Enemys.h"

using namespace std;

#define VERSION 1

bool ToDunge = false;

void Player::GetDamage(int Damage, bool UseArmor) {
	if (UseArmor) {
		Damage = max(0, Damage - MyArmor);
	}
	HP -= Damage;
	HP = min(MaxHp, HP);
	if (HP <= 0) {
		AddRecord();
		ToDunge = false;
		ReturnToHub();
	}
}

void Player::AddEffect(Effect eff, int time, int pover) {
	bool flag = false;
	for (int i = 0; i < EFFECT_BAR_SIZE; i++)
	{
		if (EffectBar[i].ID == eff.ID) {
			EffectBar[i].Time = max(EffectBar[i].Time, eff.Time);
			flag = true;
			break;
		}
	}
	if (!flag) {
		for (int i = 0; i < EFFECT_BAR_SIZE; i++)
		{
			if (EffectBar[i].ID == NULL_EFFECT) {
				EffectBar[i] = eff;
				if (pover != -1) {
					EffectBar[i].pover = pover;
				}
				if (time != -1) {
					EffectBar[i].Time = time;
				}
				break;
			}
		}
	}

}

bool Player::RemoveEffect(int id) {
	for (int i = 0; i < EFFECT_BAR_SIZE; i++)
	{
		if (EffectBar[i].ID == id) {
			EffectBar[i] = ALLEffects[0];
			return true;
		}
	}
	return false;
}



COLORREF UnknownColors[] = { RGB(255, 53, 224),RGB(46, 41, 130),RGB(61,209,75),RGB(92,6,16),RGB(12,232,217) };
HBRUSH BKBrush[] = { CreateSolidBrush(RGB(255, 0, 0)),CreateSolidBrush(RGB(255, 233, 127)),CreateSolidBrush(RGB(0, 0, 0)),CreateSolidBrush(RGB(100, 0, 0)),CreateSolidBrush(RGB(185, 255, 191)) };
Record AllRec[10];

Player FirstPlayer;

int Hub[MAP_LENTH][MAP_LENTH] = {
	{1,1,1,1,1,1,1,1,1,1,0,0,0,0,1,1,1,1,1,1,1,1,1},
	{1,0,0,0,0,0,0,0,0,0,7,7,7,7,0,0,0,0,0,0,0,0,1},
	{1,0,1,1,1,1,1,1,1,1,0,0,0,0,1,1,1,1,1,1,1,0,1},
	{1,0,1,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,1,0,1},
	{1,0,1,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,1,0,1},
	{1,0,1,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,1,0,1},
	{1,0,1,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,1,0,1},
	{1,0,1,0,0,0,0,0,0,4,0,0,0,0,1,0,0,0,0,0,1,0,1},
	{1,0,1,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,1,0,1},
	{1,0,1,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,1,0,1},
	{1,0,1,1,1,1,4,1,1,1,0,0,0,0,1,1,1,4,1,1,1,0,1},
	{1,0,0,0,0,0,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0,0,1},
	{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
	{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
	{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
	{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
	{1,0,1,1,1,4,1,1,1,1,0,0,0,0,1,1,1,1,2,1,1,0,1},
	{1,0,1,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,1,0,1},
	{1,0,1,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,1,0,1},
	{1,0,1,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,1,0,1},
	{1,0,1,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,1,0,1},
	{1,0,1,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,1,0,1},
	{1,0,1,1,1,1,1,1,1,1,0,0,0,0,1,1,1,1,1,1,1,0,1},
	{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
	{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
};

int Map[MAP_LENTH][MAP_LENTH] = {};


const int CellSize = 31 / SCREEN_SIZE * 25;
const char SavePath[] = "C:\\test\\RoudeLikeGameSave.txt";
const char RecordPath[] = "C:\\test\\RoudeLikeGameRecords.txt";



void Start(char name[]) {
	InitRandom();//������������� ������� �� ������ �������
	//setlocale(LC_ALL, "Russian");
	for (int i = 0; *(name+i)!=0&&*(name+i)!=' '; i++)
	{
		*(FirstPlayer.PlayerName + i) = *(name + i);
	}
	//strcpy_s(FirstPlayer.PlayerName, name);
	for (int i = 0; i < MAP_LENTH; i++)
	{
		for (int j = 0; j < MAP_LENTH; j++)
		{
			Map[i][j] = Hub[j][i];
		}
	}
	FirstPlayer.PlayerX = 11;
	FirstPlayer.PlayerY = 11;
}

void NewLevel() {
	EnemyCount = 0;
	for (int i = 0; i < MAP_LENTH; i++)
	{
		for (int j = 0; j < MAP_LENTH; j++)
		{
			Map[i][j] = 0;
		}
	}
	for (int i = 0; i < INVENTORY_SIZE; i++)
	{
		for (int j = 0; j < INVENTORY_SIZE; j++)
		{
			if (FirstPlayer.MyInventory[i][j].ID == KEY) {
				FirstPlayer.MyInventory[i][j] = AllItems[0];
			}
		}
	}
	FirstPlayer.PlayerX = MAP_LENTH / 2 + 7 / 2;
	FirstPlayer.PlayerY = MAP_LENTH / 2 + 7 / 2;
	FirstPlayer.HaveKey = false;
	GenerateRoom(Map);
	AddMack(Map);//�������� �� ����� �� ������� � �����������, ������� �����������
	CreateAllEnemys();
	Map[FirstPlayer.PlayerX][FirstPlayer.PlayerY] = PLAYER_ID;
}

void InitGame() {
	EnemyCount = 0;
	NewLevel();
	FirstPlayer.EffectBar[0] = ALLEffects[1];//�������� � ���������
	ShakeArray(UnknownColors, POUSHEN_COUNT);
	for (int i = 0; i < POUSHEN_COUNT; i++)
	{
		AllItems[POUSHEN_1 - NULL_ITEM + i].MyBrush = CreateSolidBrush(UnknownColors[i]);
	}
}

void ReturnToHub() {
	EnemyCount = 0;
	FirstPlayer.LastSellID = 0;
	FirstPlayer.HaveKey = false;
	for (int i = 0; i < MAP_LENTH; i++)
	{
		for (int j = 0; j < MAP_LENTH; j++)
		{
			Map[i][j] = Hub[j][i];
		}
	}
	for (int i = 0; i < INVENTORY_SIZE; i++)
	{
		for (int j = 0; j < INVENTORY_SIZE; j++)
		{
			if (FirstPlayer.MyInventory[i][j].ID != GOLDEN_COIN) {
				FirstPlayer.MyInventory[i][j] = AllItems[0];
			}
		}
	}
	for (int i = 0; i < 4; i++)
	{
		FirstPlayer.MyAmmunition[i] = AllItems[0];
	}
	for (int i = 0; i < EFFECT_BAR_SIZE; i++)
	{
		FirstPlayer.EffectBar[i] = ALLEffects[0];
	}
	FirstPlayer.HP = FirstPlayer.MaxHp;
	FirstPlayer.MyDamage = 1;
	FirstPlayer.MyArmor = 0;
	FirstPlayer.PlayerX = 11;
	FirstPlayer.PlayerY = 11;
}


void DrawMap(HDC *hdc);
void DrawInventory(HDC* hdc);
void DrawEffects(HDC* hdc);
void SaveGame();





void DrawGameBard(HDC hdc) {
	
	
	
	//����������� �������� ����
	DrawMap(&hdc);

	//����������� ���������
	DrawInventory(&hdc);

	//�������
	DrawEffects(&hdc);

	
}



void DrawMap(HDC* hdc) {
	int Screen[SCREEN_SIZE][SCREEN_SIZE] = {};
	GetPlayerScreenArea(Map, Screen, FirstPlayer.PlayerX, FirstPlayer.PlayerY);
	GenerateShadows(Map,Screen, FirstPlayer.PlayerX, FirstPlayer.PlayerY);	
	RECT rect = { 0,0,(SCREEN_SIZE)* CellSize,(SCREEN_SIZE)* CellSize };
	FillRect(*hdc, &rect, AllBlocks[WALL_ID].MyBrush);//������ ��������� ������� ������ ����� - ������ �������

	//����������� �������� ����
	for (int i = 0; i < SCREEN_SIZE; i++)
	{
		for (int j = 0; j < SCREEN_SIZE; j++)
		{
			int y = (j)*CellSize;
			int x = (i)*CellSize;

			if (Screen[i][j] > NULL_ITEM) {
				AllBlocks[0].DrowPicures(*hdc, x, y, CellSize,
					AllBlocks[0].MyBrush, -1);//������ �������� ��� ������
				AllItems[Screen[i][j] - NULL_ITEM].DrowPicures(*hdc, x, y, CellSize,
					AllItems[Screen[i][j] - NULL_ITEM].MyBrush);//������ �������
			}
			else if (Screen[i][j] != WALL_ID) {
				int qqq = -1;//���� ��� ������ ������, �� �������� ����� -1
				if (Screen[i][j] == ENEMY_ID) {
					//���� ��� ���� - �������� ��� ��� �������� � ��������� �������� �� SellSIze
					qqq = GetEnemyFromCoord(j + max(0, FirstPlayer.PlayerY - SCREEN_SIZE / 2), i + max(0, FirstPlayer.PlayerX - SCREEN_SIZE / 2))->
						GetProcent()*CellSize;//����� ���� �������� ���� �������� NULL, �� ����������� ��������� ���������� ����������. ������

				}
				AllBlocks[Screen[i][j]].DrowPicures(*hdc, x, y, CellSize,
					AllBlocks[Screen[i][j]].MyBrush, qqq);
			}
		}
	}
}

void DrawInventory(HDC* hdc) {
	int x = SCREEN_SIZE * CellSize + 40;
	int y = 25;
	RECT rect;
	if (FirstPlayer.NavigationMeny == 1&& FirstPlayer.Selectj == -1 && FirstPlayer.SelectI != -1) {
		rect = { x + (41 * FirstPlayer.SelectI) - 1,y - 1,x + (41 * FirstPlayer.SelectI) + 41,y  + 41 };
		FillRect(*hdc, &rect, BKBrush[0]);//�������� ������ ��� ������ ������������� ��� ���
		PrintInformation(*hdc, FirstPlayer.MyAmmunition[FirstPlayer.SelectI], SCREEN_SIZE * CellSize + 40, 220 + 84);
	}
	
	//������ ����������
	for (int i = 0; i < 4; i++)
	{
		
		AllItems[0].DrowPicures(*hdc, x, y, 40, AllItems[0].MyBrush);//�������� ��� ������
		if (FirstPlayer.MyAmmunition[i].ID == NULL_ITEM) {
			if (i == 0) {
				AllItems[IRON_SWORD - NULL_ITEM].DrowPicures(*hdc, x, y, 40, AllItems[0].MyBrush);
			}
			else if (i == 1) {
				AllItems[LEATHER_JACKET - NULL_ITEM].DrowPicures(*hdc, x, y, 40, AllItems[0].MyBrush);
			}
		}
		else {
			FirstPlayer.MyAmmunition[i].DrowPicures(*hdc, x, y, 40, FirstPlayer.MyAmmunition[i].MyBrush);
		}
		x += 41;
	}
	x = SCREEN_SIZE * CellSize + 40;
	y = 84;
	rect = { x,y+220 ,x + 250,y+220 + 400  };
	FillRect(*hdc, &rect, BKBrush[1]);
	if (FirstPlayer.NavigationMeny == 1 && FirstPlayer.Selectj != -1 ){
		rect = { x + (41 * FirstPlayer.SelectI) - 1,y + (41 * FirstPlayer.Selectj) - 1,x + (41 * FirstPlayer.SelectI) + 41,y + (41 * FirstPlayer.Selectj) + 41 };
		FillRect(*hdc, &rect, BKBrush[0]);
		if (FirstPlayer.MyInventory[FirstPlayer.SelectI][FirstPlayer.Selectj].ID != AllItems[0].ID) {
			PrintInformation(*hdc, FirstPlayer.MyInventory[FirstPlayer.SelectI][FirstPlayer.Selectj], SCREEN_SIZE * CellSize + 40, 220 + 84);
		}

	}
	//���������
	for (int i = 0; i < INVENTORY_SIZE; i++)
	{
		for (int j = 0; j < INVENTORY_SIZE; j++)
		{
			
			/*if (FirstPlayer.NavigationMeny == 1 && i == FirstPlayer.SelectI &&
				j == FirstPlayer.Selectj) {
				rect = { x - 1,y - 1,x + 41,y + 41 };
				FillRect(*hdc, &rect, CreateSolidBrush(RGB(255, 0, 0)));
				PrintInformation(*hdc, FirstPlayer.MyInventory[i][j], SCREEN_SIZE * CellSize + 40, 220 + 84);
				
			}*/
			if (FirstPlayer.MyInventory[i][j].ID != NULL_ITEM) {
				AllItems[0].DrowPicures(*hdc, x, y, 40,
					AllItems[0].MyBrush);//������ �������� ��� ��������� ��������
			}
			FirstPlayer.MyInventory[i][j].DrowPicures(*hdc, x, y, 40,
				FirstPlayer.MyInventory[i][j].MyBrush);//������ �������

			//���� � ��������� ������� �� �������
			if (FirstPlayer.MyInventory[i][j].count != 0) {
				char ItemCount[5];
				TCHAR  TcharItemCount[5];
				sprintf_s(ItemCount, "%d", FirstPlayer.MyInventory[i][j].count);
				OemToChar(ItemCount, TcharItemCount);
				TextOut(*hdc, x + 25, y + 20, TcharItemCount, _tcslen(TcharItemCount));
			}

			y += 41;
		}
		x += 41;
		y = 84;
	}

}

void DrawEffects(HDC* hdc) {
	int x = SCREEN_SIZE * CellSize + 40;
	int y = 0;
	RECT rect = { x ,y,x + 163 ,y + 20 };
	FillRect(*hdc, &rect, BKBrush[2]);//�������� ��� HP �������
	rect = { x ,y,x + (long)(163 * (FirstPlayer.HP*1.0 / FirstPlayer.MaxHp)) ,y + 20 };//���� �������
	FillRect(*hdc, &rect, BKBrush[3]);

	x += (INVENTORY_SIZE+1)*41;
	rect = { x,y,x + 200,y + 250 };
	FillRect(*hdc, &rect, BKBrush[4]);//�������� ��� �������
	for (int i = 0; i < EFFECT_BAR_SIZE; i++)
	{
		if (FirstPlayer.EffectBar[i].ID != NULL_EFFECT) {
			char Eff[29];
			char buff[29];
			TCHAR out[29];
			convertFromTCHAR(FirstPlayer.EffectBar[i].name, buff);
			sprintf_s(Eff, "%d %s", FirstPlayer.EffectBar[i].Time,buff);
			convertToTCHAR(Eff, out);
			SetBkMode(*hdc, TRANSPARENT);
			TextOut(*hdc, x + 5, y, out, _tcslen(out));
			y += 20;
		}
	}
	
	if (FirstPlayer.ShowEffectInfo != -1&& FirstPlayer.EffectBar[FirstPlayer.ShowEffectInfo].ID!=AllItems[0].ID) {
		x = SCREEN_SIZE * CellSize + 40;
		y = 220 + 84;
		Effect it = FirstPlayer.EffectBar[FirstPlayer.ShowEffectInfo];
		TCHAR* tm = it.name;
		SetBkMode(*hdc, TRANSPARENT);
		TextOut(*hdc, x + 30, y + 10, tm, _tcslen(tm));
		int i = 0;
		while (*tm != '\0')
		{
			tm = it.Info[i];
			TextOut(*hdc, x + 10, y + 20 * (i + 2), tm, _tcslen(tm));
			i++;
		}
	}
	if (FirstPlayer.NavigationMeny == 1 && FirstPlayer.SelectI != -1 && FirstPlayer.Selectj != -1) {
		FirstPlayer.ShowEffectInfo = -1;
	}
}


int AddInInventiry(int ID) {
	//�������� ������� � ���������
	//���� � ��������� ����� ������� ���� - ����������� �����������
	
	if (ID == KEY) {
		if (!FirstPlayer.HaveKey) {
			FirstPlayer.HaveKey = true;
		}
		else {
			return 0;
		}
	}
	//������ � ����� �� ���������
	if (!AllItems[ID - NULL_ITEM].IsWeapon && !AllItems[ID - NULL_ITEM].IsArmor) {


		for (int i = 0; i < INVENTORY_SIZE; i++)
		{
			for (int j = 0; j < INVENTORY_SIZE; j++)
			{
				if (FirstPlayer.MyInventory[i][j].ID == ID) {
					FirstPlayer.MyInventory[i][j].count += 1;
					return 0;
				}
			}
		}
	}
	//���� � ��������� ��� - ���� ��������� ������
	for (int i = 0; i < INVENTORY_SIZE; i++)
	{
		for (int j = 0; j < INVENTORY_SIZE; j++)
		{
			if (FirstPlayer.MyInventory[i][j].ID == AllItems[0].ID) {
				FirstPlayer.MyInventory[i][j] = AllItems[ID - NULL_ITEM];
				FirstPlayer.MyInventory[i][j].count = 1;
				if (FirstPlayer.MyInventory[i][j].IsWeapon) {
					FirstPlayer.MyInventory[i][j].Damage = GetRandomInt(1, FirstPlayer.MyInventory[i][j].Damage);
				}
				if (FirstPlayer.MyInventory[i][j].IsArmor) {
					FirstPlayer.MyInventory[i][j].Armor = GetRandomInt(1, FirstPlayer.MyInventory[i][j].Armor);
				}
				return 0;
			}
		}
	}
	return 1;
}



int IGoToThis(int Sx, int Sy, int Ex, int Ey) {
	//�������� ������ �� ������� ��� � ����������� �
	if (Map[Ex][Ey] == WALL_ID|| Map[Ex][Ey] ==ENEMY_ID|| Map[Ex][Ey] ==PLAYER_ID||(Map[Ex][Ey] == KEY_DOR_ID && !FirstPlayer.HaveKey)) {
		return 0;
	}
	Map[Sx][Sy] = FirstPlayer.LastSellID;
	FirstPlayer.LastSellID = 0;
	if (Map[Ex][Ey] == DOOR_ID || Map[Ex][Ey] == OPEN_DOOR_ID|| Map[Ex][Ey] == KEY_DOR_ID) {
		FirstPlayer.LastSellID = OPEN_DOOR_ID;


	}
	if (Map[Ex][Ey] == EXIT) {
		
		if (ToDunge) {
			NewLevel();
			SaveGame();
		}
		else {
			ToDunge = true;
			InitGame();
			SaveGame();
		}
		
		return 2;
	}
	if (Map[Ex][Ey] > NULL_ITEM) {
		if(AddInInventiry(Map[Ex][Ey])==1){
			FirstPlayer.LastSellID = Map[Ex][Ey];
		}
	}
	Map[Ex][Ey] = PLAYER_ID;
	return 1;
}

void PrintInformation(HDC hdc,Item it, int x, int y) {

	if (AllItems[it.ID - NULL_ITEM].count == -1) {
		it = UnknownPoushen;
	}
	SetBkMode(hdc, TRANSPARENT);
	TCHAR* tm = it.name;
	TextOut(hdc, x + 30, y + 10, tm, _tcslen(tm));
	int i = 0;
	while (*tm != '\0')
	{
		tm = it.info[i];
		TextOut(hdc, x + 10, y + 20 * (i + 2), tm, _tcslen(tm));
		i++;
	}
	i++;
	if (it.IsWeapon) {
		
		char CInfo[29];
		sprintf_s(CInfo, "���� ������ -%d", it.Damage);
		TCHAR texst[29];
		convertToTCHAR(CInfo, texst);

		TextOut(hdc, x + 10, y + 20 * (i + 2), texst, _tcslen(texst));
	}

	else if (it.IsArmor) {
		char CInfo[29];
		sprintf_s(CInfo, "������ ����� -%d", it.Armor);
		TCHAR texst[29];
		convertToTCHAR(CInfo, texst);
		TextOut(hdc, x + 10, y + 20 * (i + 2), texst, _tcslen(texst));

	}
}


void Step() {
	if (ToDunge) {
		FirstPlayer.StepCount++;
	}
	for (int i = 0; i < EFFECT_BAR_SIZE; i++)
	{
		if (FirstPlayer.EffectBar[i].ID != NULL_EFFECT) {
			FirstPlayer.EffectBar[i].Time -= 1;
			Effect Buf = FirstPlayer.EffectBar[i];
			if (FirstPlayer.EffectBar[i].Time == 0) {

				FirstPlayer.EffectBar[i] = ALLEffects[0];
			}
			Buf.Action(&FirstPlayer, Buf.Time,Buf.pover,0);
		}
	}
	MoveAllEnemy(&FirstPlayer);

}


bool MovePlayer(int dx, int dy) {
	if (FirstPlayer.NavigationMeny == 0) {
		int x = FirstPlayer.PlayerX + dx;
		int y = FirstPlayer.PlayerY + dy;
		if (x >= 0 && y >= 0 && x < MAP_LENTH && y < MAP_LENTH) {
			int out = IGoToThis(FirstPlayer.PlayerX, FirstPlayer.PlayerY, x, y);
			if (out==1) {
				FirstPlayer.PlayerX = x;
				FirstPlayer.PlayerY = y;
				Step();
			}
			if (out > 0) {
				return true;
			}
		}
		
	}
	else {
		int i = FirstPlayer.SelectI + dx;
		int j = FirstPlayer.Selectj + dy;
		if (i >= 0 && j >= -1 && i < INVENTORY_SIZE && j < INVENTORY_SIZE) {
			FirstPlayer.SelectI = i;
			FirstPlayer.Selectj = j;
			return true;
		}
	}
	return false;
}


void Attak(int dx, int dy) {
	Enemy * OnAtack = GetEnemyFromCoord(FirstPlayer.PlayerY + dy, FirstPlayer.PlayerX + dx);
	if (OnAtack != NULL) {
		if (FirstPlayer.Thi != -1 && FirstPlayer.Thj != -1) {
			FirstPlayer.MyInventory[FirstPlayer.Thi][FirstPlayer.Thj].SecondUse(&FirstPlayer,
				&FirstPlayer.MyInventory[FirstPlayer.Thi][FirstPlayer.Thj], OnAtack);
		}
		else {
			OnAtack->GetDamage(FirstPlayer.MyDamage);
		}
		Step();
	}
}


#pragma region  ������ ���
//bool MoveUp() {
//	if (FirstPlayer.NavigationMeny == 0) {
//		if (FirstPlayer.PlayerY - 1 >= 0) {
//
//			if (IGoToThis(FirstPlayer.PlayerX, FirstPlayer.PlayerY, FirstPlayer.PlayerX, FirstPlayer.PlayerY - 1)) {
//				FirstPlayer.PlayerY -= 1;
//				Step();
//				return true;
//			}
//
//		}
//	}
//	else {
//		if (FirstPlayer.Selectj >= 0) {
//			FirstPlayer.Selectj--;
//
//
//			return true;
//		}
//	}
//	return false;
//}
//
//bool MoveDown() {
//	if (FirstPlayer.NavigationMeny == 0) {
//		if (FirstPlayer.PlayerY + 1 < 70) {
//
//			if (IGoToThis(FirstPlayer.PlayerX, FirstPlayer.PlayerY, FirstPlayer.PlayerX, FirstPlayer.PlayerY + 1)) {
//				FirstPlayer.PlayerY += 1;
//				Step();
//				return true;
//			}
//
//		}
//	}
//	else {
//		if (FirstPlayer.Selectj < (INVENTORY_SIZE - 1)) {
//			FirstPlayer.Selectj++;
//			return true;
//		}
//	}
//	return false;
//}
//
//bool MoveRight() {
//	if (FirstPlayer.NavigationMeny == 0) {
//		if (FirstPlayer.PlayerX + 1 < 70) {
//
//			if (IGoToThis(FirstPlayer.PlayerX, FirstPlayer.PlayerY, FirstPlayer.PlayerX + 1, FirstPlayer.PlayerY)) {
//				FirstPlayer.PlayerX += 1;
//				Step();
//				return true;
//			}
//
//		}
//	}
//	else {
//		if (FirstPlayer.SelectI < (INVENTORY_SIZE - 1)) {
//			FirstPlayer.SelectI++;
//			
//			return true;
//		}
//	}
//	return false;
//}
//
//bool MoveLeft() {
//	if (FirstPlayer.NavigationMeny == 0) {
//		if (FirstPlayer.PlayerX - 1 >= 0) {
//
//			if (IGoToThis(FirstPlayer.PlayerX, FirstPlayer.PlayerY, FirstPlayer.PlayerX - 1, FirstPlayer.PlayerY)) {
//				FirstPlayer.PlayerX -= 1;
//				Step();
//				return true;
//			}
//
//		}
//	}
//	else {
//		if (FirstPlayer.SelectI > 0) {
//			FirstPlayer.SelectI--;
//
//			return true;
//		}
//	}
//	return false;
//}
#pragma endregion

void OpenInventory() {
	FirstPlayer.NavigationMeny = 1;
}

void CloseInventory() {
	FirstPlayer.NavigationMeny = 0;
	if (FirstPlayer.Combi != -1 && FirstPlayer.Combj != -1) {
		FirstPlayer.MyInventory[FirstPlayer.Combi][FirstPlayer.Combj].MyBrush =
			AllItems[FirstPlayer.MyInventory[FirstPlayer.Combi][FirstPlayer.Combj].ID - NULL_ITEM].MyBrush;
		FirstPlayer.Combi = -1;
		FirstPlayer.Combj = -1;
	}
}

bool UseItem() {
	if (FirstPlayer.Selectj == -1) {
		if (FirstPlayer.MyAmmunition[FirstPlayer.SelectI].ID == NULL_ITEM) {
			return false;
		}
		FirstPlayer.MyAmmunition[FirstPlayer.SelectI].FirstUse(&FirstPlayer,
			&FirstPlayer.MyAmmunition[FirstPlayer.SelectI]);
	}
	else{
		if (FirstPlayer.MyInventory[FirstPlayer.SelectI][FirstPlayer.Selectj].ID == NULL_ITEM) {
			return false;
		}
  		FirstPlayer.MyInventory[FirstPlayer.SelectI][FirstPlayer.Selectj].FirstUse(&FirstPlayer,
			&FirstPlayer.MyInventory[FirstPlayer.SelectI][FirstPlayer.Selectj]);
	}
	Step();
	return true;
}

bool SecondUseItem() {
	if (FirstPlayer.SelectI >= 0 && FirstPlayer.Selectj >= 0) {
		int NID = FirstPlayer.MyInventory[FirstPlayer.SelectI][FirstPlayer.Selectj].ID;
		if (NID >= POUSHEN_1 && NID < POUSHEN_1 + POUSHEN_COUNT) {
			if (FirstPlayer.Thi == FirstPlayer.SelectI&&FirstPlayer.Thj == FirstPlayer.Selectj) {
				FirstPlayer.MyInventory[FirstPlayer.Thi][FirstPlayer.Thj].MyBrush =
					AllItems[FirstPlayer.MyInventory[FirstPlayer.Thi][FirstPlayer.Thj].ID - NULL_ITEM].MyBrush;
				FirstPlayer.Thi = -1;
				FirstPlayer.Thi = -1;
				return true;
			}
			else {
				FirstPlayer.Thi = FirstPlayer.SelectI;
				FirstPlayer.Thj = FirstPlayer.Selectj;
				FirstPlayer.MyInventory[FirstPlayer.Thi][FirstPlayer.Thj].MyBrush = (HBRUSH)GetStockObject(NULL_BRUSH);
				return true;
			}
		}
		else {
			FirstPlayer.MyInventory[FirstPlayer.SelectI][FirstPlayer.Selectj].SecondUse(&FirstPlayer,
				&FirstPlayer.MyInventory[FirstPlayer.SelectI][FirstPlayer.Selectj], NULL);
			return true;
		}
	}
	return false;
}

bool Select(int x, int y) {
	int CellSize = 31 / SCREEN_SIZE * 25;//
	if (x >= SCREEN_SIZE * CellSize + 40 && x < SCREEN_SIZE * CellSize + 40 + (INVENTORY_SIZE) * 41) {
		if (y >= 84 && y <= (INVENTORY_SIZE) * 41 + 84) {
			x -= SCREEN_SIZE * CellSize + 40;
			y -= 84;
			if (FirstPlayer.NavigationMeny == 1&&FirstPlayer.SelectI == x / 41 &&
				FirstPlayer.Selectj == y / 41) {
				UseItem();
				return true;
			}
			else {
				FirstPlayer.SelectI = x / 41;
				FirstPlayer.Selectj = y / 41;
				FirstPlayer.NavigationMeny = 1;
				return true;
			}
			
		}
		else if (y >= 25 && y <= (25 + 41)) {
			x -= SCREEN_SIZE * CellSize + 40;
			if (FirstPlayer.NavigationMeny == 1 && FirstPlayer.SelectI == x / 41 &&
				FirstPlayer.Selectj == -1) {
				UseItem();
				return true;
			}
			else {
				FirstPlayer.SelectI = x / 41;
				FirstPlayer.Selectj = -1;
				FirstPlayer.NavigationMeny = 1;
				return true;
			}
		}

	}
	else if(x>= SCREEN_SIZE * CellSize + 40 + (INVENTORY_SIZE+1) * 41 &&
		x<= SCREEN_SIZE * CellSize + 40 + (INVENTORY_SIZE + 1) * 41+200){
		if (y >= 0 && y <= 250) {
			y = y/20;
			FirstPlayer.ShowEffectInfo = y;
			FirstPlayer.SelectI =-1;
			FirstPlayer.Selectj = -1;
			return true;
		}
	}
	else if (FirstPlayer.NavigationMeny == 1) {
		FirstPlayer.NavigationMeny = 0;
		return true;
	}
	return false;
}

int GetPlayerNavigation() {
	return FirstPlayer.NavigationMeny;
}



void SaveGame() {
	//fopen_s(f, path, "wt");
	FILE * f;
	fopen_s(&f, SavePath, "wt");
	fprintf_s(f, "%d\n", VERSION);
	fprintf_s(f, "%d %d %d %d %d\n", MAP_LENTH, CELL_SIZE, SCREEN_SIZE, INVENTORY_SIZE,EFFECT_BAR_SIZE);

	for (int i = 0; i < MAP_LENTH; i++)
	{
		for (int j = 0; j < MAP_LENTH; j++)
		{
			fprintf_s(f, "%d ", Map[i][j]);

		}
		fprintf_s(f,"\n");
	}

	for (int i = 0; i < POUSHEN_COUNT; i++)
	{
		fprintf_s(f, "%d ", (long)(UnknownColors[i]));
	}

	fprintf_s(f, "\n%d %d %d %d %d %d %d\n", FirstPlayer.PlayerX, FirstPlayer.PlayerY, FirstPlayer.LastSellID,
		FirstPlayer.HaveKey, FirstPlayer.NavigationMeny, FirstPlayer.HP, FirstPlayer.MaxHp, FirstPlayer.MyDamage, FirstPlayer.MyArmor, FirstPlayer.Exist);
	fprintf_s(f, "%d %s\n", FirstPlayer.StepCount, FirstPlayer.PlayerName);
	for (int i = 0; i < EFFECT_BAR_SIZE; i++)
	{
		fprintf_s(f, "%d %d %d\n", FirstPlayer.EffectBar[i].ID, FirstPlayer.EffectBar[i].Time, FirstPlayer.EffectBar[i].pover);
	}

	for (int j = 0; j < INVENTORY_SIZE; j++)
	{
		fprintf_s(f, "%d %d %d %d %d\n", FirstPlayer.MyAmmunition[j].ID, FirstPlayer.MyAmmunition[j].count,
			FirstPlayer.MyAmmunition[j].Damage, FirstPlayer.MyAmmunition[j].Armor, AllItems[FirstPlayer.MyAmmunition[j].ID - NULL_ITEM].count);
	}

	for (int i = 0; i < INVENTORY_SIZE; i++)
	{
		for (int j = 0; j < INVENTORY_SIZE; j++)
		{
			fprintf_s(f, "%d %d %d %d %d\n", FirstPlayer.MyInventory[i][j].ID, FirstPlayer.MyInventory[i][j].count,
				FirstPlayer.MyInventory[i][j].Damage, FirstPlayer.MyInventory[i][j].Armor, AllItems[FirstPlayer.MyInventory[i][j].ID - NULL_ITEM].count);
		}
	}

	fprintf_s(f, "%d\n", EnemyCount);
	for (int i = 0; i < EnemyCount; i++)
	{
		fprintf(f, "%d %d %d %d %d\n", AllEnemys[i].x, AllEnemys[i].y, AllEnemys[i].Type, AllEnemys[i].LastBlock, AllEnemys[i].OnPath);
		for (int j = 0; j <= AllEnemys[i].OnPath; j++)
		{
			fprintf(f, "%d,%d ", AllEnemys[i].Path[j][0], AllEnemys[i].Path[j][1]);
		}
		if (AllEnemys[i].OnPath >= 0) {
			fprintf(f, "\n");
		}
		fprintf(f, "%d %d %d %d\n", AllEnemys[i].HP, AllEnemys[i].maxHp, AllEnemys[i].Die, AllEnemys[i].Damage);
		for (int j = 0; j < 50; j++)
		{
			fprintf_s(f, "%d %d %d\n", AllEnemys[i].EffectBar[j].ID, AllEnemys[i].EffectBar[j].Time, AllEnemys[i].EffectBar[j].pover);
		}
	}
	fclose(f);
}


bool LoadGame() {
	FILE * f;
	fopen_s(&f, SavePath, "rt");
	if (f == NULL) {
		return false;
	}
	int tmp[6] = {};
	fscanf_s(f, "%d", tmp);
	fscanf_s(f, "%d %d %d %d %d", tmp+1,tmp+2,tmp+3,tmp+4,tmp+5);
	if (tmp[0] == VERSION && tmp[1] == MAP_LENTH && tmp[2] == CELL_SIZE && tmp[3] == SCREEN_SIZE && tmp[4] == INVENTORY_SIZE && tmp[5] == EFFECT_BAR_SIZE) {
		for (int i = 0; i < MAP_LENTH; i++)
		{
			for (int j = 0; j < MAP_LENTH; j++)
			{
				fscanf_s(f, "%d ", &Map[i][j]);

			}
		}

		for (int i = 0; i < POUSHEN_COUNT; i++)
		{
			long c;
			fscanf_s(f, "%d ", &c);
			UnknownColors[i] = (COLORREF)(c);
			AllItems[POUSHEN_1 - NULL_ITEM + i].MyBrush = CreateSolidBrush(UnknownColors[i]);

		}

		fscanf_s(f, "%d %d %d %d %d %d %d", &FirstPlayer.PlayerX, &FirstPlayer.PlayerY, &FirstPlayer.LastSellID,
			&FirstPlayer.HaveKey, &FirstPlayer.NavigationMeny, &FirstPlayer.HP, &FirstPlayer.MaxHp, &FirstPlayer.MyDamage, &FirstPlayer.MyArmor, &FirstPlayer.Exist);
		fscanf_s(f, "%d %s", &FirstPlayer.StepCount, FirstPlayer.PlayerName, 100);
		for (int i = 0; i < EFFECT_BAR_SIZE; i++)
		{
			fscanf_s(f, "%d %d %d", tmp , tmp + 1, tmp + 2);
			FirstPlayer.EffectBar[i] = ALLEffects[tmp[0] - NULL_EFFECT];
			FirstPlayer.EffectBar[i].Time = tmp[1];
			FirstPlayer.EffectBar[i].pover = tmp[2];

		}

		for (int j = 0; j < INVENTORY_SIZE; j++)
		{
			fscanf_s(f, "%d %d %d %d %d", tmp, tmp + 1, tmp + 2, tmp + 3, tmp + 4);
			FirstPlayer.MyAmmunition[j] = AllItems[tmp[0] - NULL_ITEM];
			FirstPlayer.MyAmmunition[j].count = tmp[1];
			FirstPlayer.MyAmmunition[j].Damage = tmp[2];
			FirstPlayer.MyAmmunition[j].Armor = tmp[3];
			AllItems[tmp[0] - NULL_ITEM].count = tmp[4];
		}

		for (int i = 0; i < INVENTORY_SIZE; i++)
		{
			for (int j = 0; j < INVENTORY_SIZE; j++)
			{
				fscanf_s(f, "%d %d %d %d %d", tmp,tmp+1,tmp+2,tmp+3,tmp+4);
				FirstPlayer.MyInventory[i][j] = AllItems[tmp[0] - NULL_ITEM];
				FirstPlayer.MyInventory[i][j].count = tmp[1];
				FirstPlayer.MyInventory[i][j].Damage = tmp[2];
				FirstPlayer.MyInventory[i][j].Armor = tmp[3];
				AllItems[tmp[0] - NULL_ITEM].count = tmp[4];
			}
		}

		fscanf_s(f, "%d", &EnemyCount);
		for (int i = 0; i < EnemyCount; i++)
		{
			fscanf_s(f, "%d %d %d %d %d", &AllEnemys[i].x, &AllEnemys[i].y, &AllEnemys[i].Type, &AllEnemys[i].LastBlock, &AllEnemys[i].OnPath);
			for (int j = 0; j <= AllEnemys[i].OnPath; j++)
			{
				fscanf_s(f, "%d,%d ", &AllEnemys[i].Path[j][0], &AllEnemys[i].Path[j][1]);
			}
			fscanf_s(f, "%d %d %d %d\n", &AllEnemys[i].HP, &AllEnemys[i].maxHp, &AllEnemys[i].Die, &AllEnemys[i].Damage);

			for (int j = 0; j < 50; j++)
			{
				fscanf_s(f, "%d %d %d", tmp, tmp + 1, tmp + 2);
				AllEnemys[i].EffectBar[j] = ALLEffects[tmp[0] - NULL_EFFECT];
				AllEnemys[i].EffectBar[j].Time = tmp[1];
				AllEnemys[i].EffectBar[j].pover = tmp[2];

			}
			
		}
		

		fclose(f);
		return true;
	}
	fclose(f);
	return false;
	
}

void AddRecord() {
	FILE* f;
	fopen_s(&f, RecordPath, "a");
	int Gcount = 0;
	for (int i = 0; i < INVENTORY_SIZE; i++)
	{
		for (int j = 0; j < INVENTORY_SIZE; j++)
		{
			if (FirstPlayer.MyInventory[i][j].ID == GOLDEN_COIN) {
				Gcount += FirstPlayer.MyInventory[i][j].count;
			}
		}
	}
	int Ncount = 0;
	char* tmp = FirstPlayer.PlayerName;
	while (*tmp != 0||*tmp==' ') {
		Ncount += *tmp;
		tmp++;
	}
	InitRandom(Gcount + Ncount + FirstPlayer.StepCount);
	int Key = (GetRandomInt(0,50000) + GetRandomInt(0,40000)) * (GetRandomInt(1,100));
	fprintf_s(f, "%d %d %u %s\n", Gcount, FirstPlayer.StepCount, Key, FirstPlayer.PlayerName);
	fclose(f);
}

void LoadRecords(HDC hdc) {
	FILE* f;
	fopen_s(&f, RecordPath, "rt");
	Record tmp;
	char buf[150];
	while (fgets(buf, 150, f) != NULL)
	{
		sscanf_s(buf, "%d %d %u %s", &tmp.Gold, &tmp.Step, &tmp.Key, tmp.Pname, 100);
		int Ncount = 0;
		char* str = tmp.Pname;
		while (*str != 0 || *str == ' ') {
			Ncount += *str;
			str++;
		}
		InitRandom(tmp.Gold + Ncount + tmp.Step);
		int Key = (GetRandomInt(0, 50000) + GetRandomInt(0, 40000)) * (GetRandomInt(1, 100));
		if (tmp.Key == Key) {
			Record t;
			for (int i = 0; i < 10; i++)
			{
				if (AllRec[i].Gold <= tmp.Gold) {
					t = AllRec[i];
					AllRec[i] = tmp;
					tmp = t;
				}
			}
		}
	}
	fclose(f);
	RECT rect = { 0,0,500,500 };
	FillRect(hdc, &rect, (HBRUSH)(GetStockObject(DKGRAY_BRUSH)));
	HBRUSH now = (HBRUSH)GetStockObject(BLACK_BRUSH);
	GetObject(hdc, 0, now);
	SelectObject(hdc, GetStockObject(NULL_BRUSH));
	SetBkColor(hdc, RGB(86, 86, 86));
	for (int i = 0; i < 10; i++)
	{
		
		if (!strcmp(AllRec[i].Pname, "") ){
			break;
		}
		char Str[150];
		sprintf_s(Str, "������: #%d ������: %d �����: %d ���: %s", i, AllRec[i].Gold,
			AllRec[i].Step, AllRec[i].Pname);
		TCHAR res[150];
		convertToTCHAR(Str, res);
		SetBkMode(hdc, TRANSPARENT);
		TextOut(hdc, 50, 20 * i, res, strlen(Str));

	}
	SelectObject(hdc, now);
}


