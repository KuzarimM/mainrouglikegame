#pragma once

#include <string>
using namespace std;
#define MAP_LENTH 7*10
#define CELL_SIZE 7
#define SCREEN_SIZE 31
#define POUSHEN_COUNT 5

#define WALL_ID 1
#define DOOR_ID 2
#define OPEN_DOOR_ID 3
#define KEY_DOR_ID 4
#define PLAYER_ID 5
#define ENEMY_ID 6
#define EXIT 7

#define NULL_ITEM 20
#define GOLDEN_COIN 21
#define IRON_SWORD 22
#define LEATHER_JACKET 23
#define KEY 24
#define POUSHEN_1 25
#define POUSHEN_2 POUSHEN_1+1
#define POUSHEN_3 POUSHEN_1+2
#define POUSHEN_4 POUSHEN_1+3
#define POUSHEN_5 POUSHEN_1+4
#define IDENTIGRAF POUSHEN_1+POUSHEN_COUNT

#define NULL_EFFECT 100
#define SATURATUON NULL_EFFECT+1
#define HUNGER NULL_EFFECT+2
#define EXHAUSTION NULL_EFFECT+3
#define REGENERATION NULL_EFFECT+4
#define UPGRADE NULL_EFFECT+5



//#define ITEM_COUNT 4


void GetPlayerScreenArea(int MainArr[][MAP_LENTH], int OutArr[][SCREEN_SIZE], int CenterI, int CenterJ);

//void GnerateRandomRoom(int ountput[][7], int exet[], int exetcount, int Doors[][2]);
//
//void GenerateRandomTonel(int ountput[][7]);
//
//void GenerateRandomMap(int Map[][MAP_LENTH], int Len);

void GenerateRoom(int Map[][MAP_LENTH]);

void AddMack(int Map[][MAP_LENTH]);

void GenerateShadows(int SMap[][MAP_LENTH], int OutputMap[][SCREEN_SIZE], int Playeri, int Playerj);

void ShakeArray(COLORREF *Arr, int lenth);