// RougLikeGame.cpp : Определяет точку входа для приложения.
//

#include "stdafx.h"
#include "RougLikeGame.h"
#include "GameContriller.h"
#include "windowsx.h"

#define MAX_LOADSTRING 100

// Глобальные переменные:
HINSTANCE hInst;                                // текущий экземпляр
WCHAR szTitle[MAX_LOADSTRING];                  // Текст строки заголовка
WCHAR szWindowClass[MAX_LOADSTRING];            // имя класса главного окна

RECT Window;

// Отправить объявления функций, включенных в этот модуль кода:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Разместите код здесь.
	//Start();
    // Инициализация глобальных строк
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_ROUGLIKEGAME, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Выполнить инициализацию приложения:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_ROUGLIKEGAME));

    MSG msg;

    // Цикл основного сообщения:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  ФУНКЦИЯ: MyRegisterClass()
//
//  ЦЕЛЬ: Регистрирует класс окна.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ROUGLIKEGAME));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_ROUGLIKEGAME);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   ФУНКЦИЯ: InitInstance(HINSTANCE, int)
//
//   ЦЕЛЬ: Сохраняет маркер экземпляра и создает главное окно
//
//   КОММЕНТАРИИ:
//
//        В этой функции маркер экземпляра сохраняется в глобальной переменной, а также
//        создается и выводится главное окно программы.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Сохранить маркер экземпляра в глобальной переменной

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);
   
   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  ФУНКЦИЯ: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  ЦЕЛЬ: Обрабатывает сообщения в главном окне.
//
//  WM_COMMAND  - обработать меню приложения
//  WM_PAINT    - Отрисовка главного окна
//  WM_DESTROY  - отправить сообщение о выходе и вернуться
//
//
int y=0;
int x=0;

//Кнопки меню(по порядку)
HWND BSatart;
HWND InputField;
HWND BLoad;
HWND BRecord;
HWND BExit;

//Константы для меню
const TCHAR MName[] = _T("МЕНЮ");
HBRUSH BKMenu = CreateSolidBrush(RGB(217, 242, 162));
RECT MenuRect = { 200,100-80,600,700-80 };


bool MenuIsOpen = false;//Флаг для меню
bool RecordsPage = false;//Флаг для таблицы лидеров
bool InitGame = false;//Флаг запуска игры

unsigned long LastTime = 0;//Таймер манипуляций с меню

void ShowMenu(HDC hdc) {
    //Выводит всё что связано с меню
        FillRect(hdc, &MenuRect, BKMenu);
        TextOut(hdc, 350, 100-80, MName, 5);
        LastTime = clock() + CLOCKS_PER_SEC;
}

void OpenMenu() {
    //Открывает меню
    MenuIsOpen = true;
    ShowWindow(BSatart, SW_SHOWNORMAL);
    ShowWindow(InputField, SW_SHOWNORMAL);
    ShowWindow(BLoad, SW_SHOWNORMAL);
    ShowWindow(BRecord, SW_SHOWNORMAL);
    ShowWindow(BExit, SW_SHOWNORMAL);
    LastTime = clock() + CLOCKS_PER_SEC/100000;
}

void CloseMenu() {
    //Закрывает меню
        ShowWindow(BSatart, SW_HIDE);
        ShowWindow(InputField, SW_HIDE);
        ShowWindow(BLoad, SW_HIDE);
        ShowWindow(BRecord, SW_HIDE);
        ShowWindow(BExit, SW_HIDE);
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    bool UpdateFlag = false;//Флаг обновления экрана
    switch (message)
    {
    case WM_COMMAND:
        {
        if (lParam == (LPARAM)(BSatart)) {

            TCHAR Pname[100];
            GetWindowText(InputField, Pname, sizeof(Pname));
            char name[100] = "";
            convertFromTCHAR(Pname, name);
            //Получив то, что пользователь ввёл
            Start(name);
            //Запускаем игру и всё сворачиваем
            InitGame = true;
            CloseMenu();
            MenuIsOpen = false;
            InvalidateRect(hWnd, NULL, TRUE);
        }
        else if (lParam == (LPARAM)(BLoad)) {
            if (LoadGame()) {
                CloseMenu();
                MenuIsOpen = false;
                InvalidateRect(hWnd, NULL, TRUE);
                InitGame = true;
                
            }
            SetFocus(hWnd);
        }
        else if (lParam == (LPARAM)(BRecord)) {
            RecordsPage = true;
            CloseMenu();
            InvalidateRect(hWnd, NULL, TRUE);

        }
        else if (lParam == (LPARAM)(BExit)) {
            SaveGame();
            PostQuitMessage(WM_QUIT);
        }
            int wmId = LOWORD(wParam);
            // Разобрать выбор в меню:
            switch (wmId)
            {
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_CREATE:
        hInst = ((LPCREATESTRUCT)lParam)->hInstance;
        InputField = CreateWindowW(_T("edit"), _T("Введите имя"),
            WS_CHILD | WS_VISIBLE | WS_BORDER | ES_CENTER, 200, 200-80, 400, 50, hWnd, 0, hInst, NULL);
        BSatart = CreateWindowW(_T("button"), _T("Начать новую игру"),
            WS_CHILD | WS_VISIBLE | WS_BORDER, 300, 300 - 80, 200, 50, hWnd, 0, hInst, NULL);
        BLoad = CreateWindowW(_T("button"), _T("Загрузить игру"),
            WS_CHILD | WS_VISIBLE | WS_BORDER, 300, 400 - 80, 200, 50, hWnd, 0, hInst, NULL);
        BRecord = CreateWindowW(_T("button"), _T("Таблица рекордов"),
            WS_CHILD | WS_VISIBLE | WS_BORDER, 300, 500 - 80, 200, 50, hWnd, 0, hInst, NULL);
        BExit = CreateWindowW(_T("button"), _T("Выход"),
            WS_CHILD | WS_VISIBLE | WS_BORDER, 300, 600 - 80, 200, 50, hWnd, 0, hInst, NULL);
        ShowWindow(BSatart, SW_HIDE);
        ShowWindow(InputField, SW_HIDE);
        ShowWindow(BLoad, SW_HIDE);
        ShowWindow(BRecord, SW_HIDE);
        ShowWindow(BExit, SW_HIDE);
        OpenMenu();
        break;
	case WM_SIZE:
		GetClientRect(hWnd, &Window);
		break;
	case WM_LBUTTONDOWN:
        if (MenuIsOpen||RecordsPage) {
            break;
        }
		x = GET_X_LPARAM(lParam);
		y = GET_Y_LPARAM(lParam);
		UpdateFlag = Select(x, y);
		break;
	case WM_KEYDOWN:
     
        if (MenuIsOpen || RecordsPage) {
            
            if (wParam == VK_ESCAPE&& clock()>LastTime) {
              
                if(RecordsPage){
                    RecordsPage = false;
                    if (MenuIsOpen) {
                        OpenMenu();
                    }
                }
                else{
                CloseMenu();
                MenuIsOpen = false;
                }
                InvalidateRect(hWnd, NULL, TRUE);
            }
            break;
        }
        switch (wParam)
        {
        case VK_LEFT:
            UpdateFlag = MovePlayer(-1, 0);
            break;
        case VK_RIGHT:
            UpdateFlag = MovePlayer(1, 0);
            break;
        case VK_UP:
            UpdateFlag = MovePlayer(0, -1);
            break;
        case VK_DOWN:
            UpdateFlag = MovePlayer(0, 1);
            break;
        case VK_NUMPAD8:
            Attak(0, -1);
            UpdateFlag = true;
            break;
        case VK_NUMPAD2:
            Attak(0, 1);
            UpdateFlag = true;
            break;
        case VK_NUMPAD4:
            Attak(-1, 0);
            UpdateFlag = true;
            break;
        case VK_NUMPAD6:
            Attak(1, 0);
            UpdateFlag = true;
            break;
        case 80://P
            Step();
            UpdateFlag = true;
            break;
        case 82://P
            UpdateFlag = SecondUseItem();
            break;
        case 73://Код кнопки I

            if (GetPlayerNavigation() == 0) {
                OpenInventory();
            }
            else {
                CloseInventory();
            }
            UpdateFlag = true;
            break;
        case VK_ESCAPE:
            OpenMenu();
            UpdateFlag = true;
			break;
		case 0x45://Код кнопки E
			if (GetPlayerNavigation() == 1) {
				UpdateFlag = UseItem();
			}
			break;
		}
		break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);

			HDC Buffer = CreateCompatibleDC(hdc);//Создаём ещё один hdc совместимый с основным
			HBITMAP memBM = CreateCompatibleBitmap(hdc, Window.right - Window.left,Window.bottom-Window.top);//Создаём область рисования совместимую с hdc, по размерам экрана
			SelectObject(Buffer, memBM);//Применяем область рисования на новый hdc
			RECT All = { 0,0,Window.right - Window.left,Window.bottom - Window.top };
			FillRect(Buffer, &All, GetStockBrush(WHITE_BRUSH));//По некторым причинам новый hdc-в оригенале с чёрным фоном. Это можно поправить нарисовав прямоугольник белым

            if (InitGame&&!RecordsPage) {
                DrawGameBard(Buffer);
            }
            if (MenuIsOpen&& !RecordsPage) {
                ShowMenu(Buffer);
            }
            if (RecordsPage) {
                LoadRecords(Buffer);
            }
			BitBlt(hdc, 0, 0, Window.right - Window.left, Window.bottom - Window.top, Buffer, 0, 0, SRCCOPY);//Побитовый перенос данных одного контекста hdc в основной
            //AllItems[5].DrowPicures(hdc, 100, 100, 25, AllItems[5].MyBrush);

            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
	if (UpdateFlag) {
		InvalidateRect(hWnd, NULL, FALSE);
	}
    return 0;
}

// Обработчик сообщений для окна "О программе".
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}
