#pragma once

#include "stdafx.h"
#include "RougLikeGame.h"
#include <stdio.h>
#include <ctime>
#include "GameContriller.h"
#include "MapGenerator.h"
#include"PIHelper.h"
#include "Entities.h"


#define INVENTORY_SIZE 4
#define EFFECT_BAR_SIZE 10


struct Player
{
	int PlayerX = MAP_LENTH / 2 + 7 / 2;
	int PlayerY = MAP_LENTH / 2 + 7 / 2;
	int LastSellID = 0;
	Item MyInventory[INVENTORY_SIZE][INVENTORY_SIZE] = {};
	Item MyAmmunition[4] = {};
	bool HaveKey = false;
	int NavigationMeny = 0;
	int SelectI = 0;
	int Selectj = 0;
	int Combi = -1;
	int Combj = -1;
	int Thi = -1;
	int Thj = -1;
	int ShowEffectInfo = -1;
	
	int HP = 100;
	int MaxHp = 100;
	int MyDamage = 1;
	int MyArmor = 0;
	bool Exist = true;
	Effect EffectBar[EFFECT_BAR_SIZE];

	char PlayerName[100];
	int StepCount = 0;
	
	void GetDamage(int Damage, bool UseArmor);
	void AddEffect(Effect eff, int time = -1, int pover = -1);
	bool RemoveEffect(int id);
};

struct Record {
	int Gold;
	int Step;
	char Pname[100];
	unsigned int Key;
};

extern int Map[MAP_LENTH][MAP_LENTH];

void Start(char name[]);

void DrawGameBard(HDC hdc);

bool MovePlayer(int dx, int dy);

bool MoveDown();

bool MoveUp();

bool MoveRight();

bool MoveLeft();

void OpenInventory();

void CloseInventory();

int GetPlayerNavigation();

bool UseItem();

bool SecondUseItem();

void Attak(int dx, int dy);

void Step();

bool Select(int x, int y);

void SaveGame();

bool LoadGame();

void LoadRecords(HDC hdc);

void ReturnToHub();

void AddRecord();

void PrintInformation(HDC hdc, Item it, int x, int y);