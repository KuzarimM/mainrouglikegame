
#include "stdafx.h"
#include "Entities.h";
#include "GameContriller.h"
#include "Enemys.h"

char Ajetivs[12][30] = {
	"��������������",
	"������������",
	"�����������������",
	"���������",
	"������",
	"�����������",
	"Python������",
	"������������",
	"���������������",
	"��������",
	"�������������",
	"����������"
};

char Noun[13][30]{
	"���",
	"�������",
	"��������",
	"��������",
	"�������",
	"����",
	"�������",
	"������",
	"�������� ����",
	"����",
	"������",
	"���",
	"����"
};

#pragma region �����
void NullBl(HDC hdc, int x, int y, int CellSize, HBRUSH hBrush, int par);
void Wall(HDC hdc, int x, int y, int CellSize, HBRUSH hBrush, int par);
void Door(HDC hdc, int x, int y, int CellSize, HBRUSH hBrush, int par);
void OpenedD(HDC hdc, int x, int y, int CellSize, HBRUSH hBrush, int par);
void KeyDoor(HDC hdc, int x, int y, int CellSize, HBRUSH hBrush, int par);
void PlayerID(HDC hdc, int x, int y, int CellSize, HBRUSH hBrush, int par);
void DrowEnemy(HDC hdc, int x, int y, int CellSize, HBRUSH hBrush, int par);
void DrawExit(HDC hdc, int x, int y, int CellSize, HBRUSH hBrush, int par);

Block NullBlock = { 0,NullBl,CreateSolidBrush(RGB(96, 96, 96)) };
Block WallBlock = { WALL_ID,Wall,CreateSolidBrush(RGB(0, 0, 0)) };
Block ClosedDoorBlock = { DOOR_ID,Door,CreateSolidBrush(RGB(71, 46, 11)) };
Block OpenedDoolBlock = { OPEN_DOOR_ID,OpenedD,CreateSolidBrush(RGB(71, 46, 11)) };
Block KeyClosedDoorBlock = { KEY_DOR_ID,Door,CreateSolidBrush(RGB(111,130,138)) };
Block PlayerBlock = { PLAYER_ID,PlayerID, CreateSolidBrush(RGB(0, 38, 255)) };
Block EnemyBlock = { ENEMY_ID,DrowEnemy, CreateSolidBrush(RGB(255, 0, 0)) };
Block ExitBlock = { EXIT,DrawExit, CreateSolidBrush(RGB(0, 255, 255)) };
#pragma endregion



#pragma region ������ ��������� ������
void NullBl(HDC hdc, int x, int y, int CellSize, HBRUSH hBrush, int par) {
	RECT rect = { x,y,x + CellSize,y + CellSize };
	FillRect(hdc, &rect, hBrush);
}

void Wall(HDC hdc, int x, int y, int CellSize, HBRUSH hBrush, int par) {
	RECT rect = { x,y,x + CellSize,y + CellSize };
	FillRect(hdc, &rect, hBrush);
}

void Door(HDC hdc, int x, int y, int CellSize, HBRUSH hBrush, int par) {

	RECT rect = { x,y,x + CellSize,y + CellSize };
	//FillRect(hdc, &rect, NullBlock.MyBrush);
	FillRect(hdc, &rect, WallBlock.MyBrush);
	
	HBRUSH now = hBrush;
	GetObject(hdc, 0, now);
	SelectObject(hdc, hBrush);
	Ellipse(hdc, x, y, x + CellSize, y + CellSize);
	rect = { x+1, y + CellSize / 2, x + CellSize-1, y + CellSize };
	FillRect(hdc, &rect, hBrush);
	SelectObject(hdc, now);
	MoveToEx(hdc, x, y + CellSize / 2, NULL);
	LineTo(hdc, x, y + CellSize-1);
	LineTo(hdc, x + CellSize-1, y + CellSize-1);
	LineTo(hdc, x + CellSize-1, y + CellSize / 2);
	rect = { x,y + CellSize / 2,x + CellSize / 10 * 3,y + CellSize / 10 * 7 };
	FillRect(hdc, &rect, (HBRUSH)(GetStockObject(GRAY_BRUSH)));
	rect = { x,y + CellSize / 5 * 4, x + CellSize / 10 * 3,y+CellSize/10*9 };
	FillRect(hdc, &rect, (HBRUSH)(GetStockObject(GRAY_BRUSH)));
	rect = { x + CellSize / 10 * 7,y + CellSize / 5 * 3, x + CellSize / 10 * 9,
		y + CellSize / 5 * 4 };
	FillRect(hdc, &rect, (HBRUSH)(GetStockObject(GRAY_BRUSH)));

}

void OpenedD(HDC hdc, int x, int y, int CellSize, HBRUSH hBrush, int par) {
	RECT rect = { x,y,x + CellSize,y + CellSize };
	
	FillRect(hdc, &rect, NullBlock.MyBrush);
	rect = { x,y,x + CellSize /5 *2,y + CellSize };
	FillRect(hdc, &rect, hBrush);
	rect = { x,y + CellSize / 2,x + CellSize / 10 * 4,y + CellSize / 10 * 7 };
	FillRect(hdc, &rect, (HBRUSH)(GetStockObject(GRAY_BRUSH)));
	rect = { x,y + CellSize / 5 * 4, x + CellSize / 10 * 4,y + CellSize / 10 * 9 };
	FillRect(hdc, &rect, (HBRUSH)(GetStockObject(GRAY_BRUSH)));
}

void KeyDoor(HDC hdc, int x, int y, int CellSize, HBRUSH hBrush, int par) {
	RECT rect = { x,y,x + CellSize,y + CellSize };
	FillRect(hdc, &rect, hBrush);
}

void PlayerID(HDC hdc, int x, int y, int CellSize, HBRUSH hBrush, int par) {
	RECT rect = { x,y,x + CellSize,y + CellSize };
	FillRect(hdc, &rect, hBrush);
}

void DrowEnemy(HDC hdc, int x, int y, int CellSize, HBRUSH hBrush, int par) {
	RECT rect = { x,y,x + CellSize,y + CellSize };
	FillRect(hdc, &rect, hBrush);
	HBRUSH now = hBrush;
	GetObject(hdc, 0, now);
	SelectObject(hdc, GetStockObject(BLACK_BRUSH));
	Rectangle(hdc,x, y + CellSize / 4, x +par, y + CellSize / 4 * 3);
	SelectObject(hdc,now);

}

void DrawExit(HDC hdc, int x, int y, int CellSize, HBRUSH hBrush, int par) {
	RECT rect = { x,y,x + CellSize,y + CellSize };
	FillRect(hdc, &rect, hBrush);
}

#pragma endregion



Block AllBlocks[] = { NullBlock,WallBlock,ClosedDoorBlock,OpenedDoolBlock,KeyClosedDoorBlock,PlayerBlock,EnemyBlock,ExitBlock};


#pragma region ����� ��������� ���������
void NullIt(HDC hdc, int x, int y, int CellSize, HBRUSH hBrush) {
	RECT rect = { x,y,x + CellSize,y + CellSize };
	FillRect(hdc, &rect, hBrush);
}

void GoldCin(HDC hdc, int x, int y, int CellSize, HBRUSH hBrush) {
	Ellipse(hdc, x, y, x + CellSize, y + CellSize);
	HBRUSH now = hBrush;
	GetObject(hdc, 0, now);
	SelectObject(hdc, hBrush);
	ExtFloodFill(hdc, x + CellSize / 2, y + CellSize / 2, RGB(0, 0, 0), FLOODFILLBORDER);
	SelectObject(hdc, now);
}

void IronSvrd(HDC hdc, int x, int y, int CellSize, HBRUSH hBrush) {
	x = x + CellSize / 2;
	y = y + CellSize / 2;
	HBRUSH now = hBrush;
	GetObject(hdc, 0, now);
	SelectObject(hdc, hBrush);
	int d = 1;
	POINT blade[] = {
		x + CellSize / 5 * 2- d, y - CellSize / 2+ d,
		x + CellSize / 2- d, y - CellSize / 2+ d,
		x + CellSize / 2- d, y - CellSize / 5 * 2+ d,
		x - CellSize / 5 * 2+d, y + CellSize / 2-d,
		x - CellSize /2+ d, y + CellSize / 5 * 2- d,
		x + CellSize / 5 * 2- d, y - CellSize / 2 + d
	};
	Polygon(hdc, blade, 6);

	POINT Guard[] = {
		x - CellSize / 5 * 2,y,
		x ,y + CellSize / 5 * 2,
		x-CellSize/10,y + CellSize / 5 * 2,
		x - CellSize / 5 * 2,y+CellSize/10,
		x - CellSize / 5 * 2,y,
	};
	Polygon(hdc, Guard, 5);
	
	SelectObject(hdc, now);
}

void Poush(HDC hdc, int x, int y, int CellSize, HBRUSH hBrush) {
	x = x + CellSize / 2;
	y = y + CellSize / 2;
	HBRUSH now = hBrush;
	GetObject(hdc, 0, now);
	SelectObject(hdc, hBrush);
	int d = 1;
	POINT flask[] = {
		x + CellSize / 10 * 3,y - CellSize / 5,
		x + CellSize / 5 * 2,y - CellSize / 10,
		x + CellSize / 5 * 2,y + CellSize / 10 * 3,
		x + CellSize / 5,y + CellSize /2 -d,
		x - CellSize / 5,y + CellSize /2 -d,
		x - CellSize / 5 * 2,y + CellSize / 10 * 3,
		x - CellSize / 5 * 2,y - CellSize / 10,
		x - CellSize / 10 * 3,y - CellSize / 5,
		x + CellSize / 10 * 3,y - CellSize / 5,

	};
	Polyline(hdc, flask, 9);
	ExtFloodFill(hdc, x, y, RGB(0, 0, 0), FLOODFILLBORDER);
	Rectangle(hdc, x - CellSize / 10, y - CellSize / 2, x + CellSize / 10, y - CellSize / 5);
	SelectObject(hdc, now);
}

void IronKey(HDC hdc, int x, int y, int CellSize, HBRUSH hBrush) {
	x = x + CellSize / 2;
	y = y + CellSize / 2;
	HBRUSH now = hBrush;
	GetObject(hdc, 0, now);
	SelectObject(hdc, hBrush);
	int d = 2;
	POINT Rod[] = {
		x-CellSize/10,y-CellSize/5,
		x-CellSize/10,y+CellSize/5,
		x-CellSize/10*3,y+CellSize/5,
		x-CellSize/10*3,y+CellSize/5*2,
		x-CellSize/10,y+CellSize/5*2,
		x-CellSize/10,y+CellSize/2-d,
		x+CellSize/10,y+CellSize/2-d,
		x+CellSize/10,y - CellSize / 5,
		x - CellSize / 10,y - CellSize / 5,
	};
	Polyline(hdc, Rod, 9);
	ExtFloodFill(hdc, x, y, RGB(0, 0, 0), FLOODFILLBORDER);
	Ellipse(hdc, x - CellSize / 5, y - CellSize /2, x + CellSize / 5, y - CellSize /10);
	


	SelectObject(hdc, now);
}

void LJacket(HDC hdc, int x, int y, int CellSize, HBRUSH hBrush) {
	x = x + CellSize / 2;
	y = y + CellSize/ 2;
	HBRUSH now = hBrush;
	GetObject(hdc, 0, now);
	SelectObject(hdc, hBrush);
	POINT MPart[] = {
		x - CellSize / 10 * 3,y - CellSize / 2,
		x + CellSize / 10 * 3,y - CellSize / 2,
		x + CellSize / 2-1,y - CellSize / 5,
		x + CellSize /2-1 ,y,
		x + CellSize / 10 * 3,y - CellSize / 5,
		x + CellSize / 10 * 3,y + CellSize / 5 * 2,
		x - CellSize / 10 * 3,y + CellSize / 5 * 2,
		x - CellSize / 10 * 3,y - CellSize / 5,
		x - CellSize / 2+1,y,
		x - CellSize / 2+1,y - CellSize / 5,
	};
	Polygon(hdc, MPart, 10);
	//HBRUSH HB = CreateSolidBrush(RGB(148, 93, 11));
	//SelectObject(hdc, HB);
	Rectangle(hdc, x - CellSize / 10 * 3, y , x + CellSize /10*3+1, y + CellSize / 5);
	SelectObject(hdc, now);
	//DeleteObject(HB);
}

void Ident(HDC hdc, int x, int y, int CellSize, HBRUSH hBrush) {
	HBRUSH now = hBrush;
	GetObject(hdc, 0, now);
	SelectObject(hdc, hBrush);
	Rectangle(hdc, x + CellSize / 4, y + CellSize / 4, x + CellSize / 4 * 3, y + CellSize / 4 * 3);
	SelectObject(hdc, now);
}
#pragma endregion


#pragma region  ��������
void DoingNothing(void* player, void* thItem);
void DoingNothing2(void* player, void* thItem, void*enemy);
void DoingEqip(void* player, void* thItem);
void DoingReneme(void* player, void * thItem, void* enemy);
void DoingDrinkPoushen(void* player, void* thItem);
void DoingThrowPoushen(void* player, void * thItem, void* enemy);
void DoingIdentifi(void* player, void* thItem);

Item NullItem;


Item GoldenCoin = {
	GOLDEN_COIN,
	GoldCin,
	_T("�������� ������"),
	{
		_T("��������� ������� �������")
		,_T("��������� ����� �����."),
		_T("�������� �� ��� ��� �����"),
		_T("��������� �� �������� ��"),
		_T("������� ������� ����"),
		_T("�������� ������������������"),
		_T("����.")
	},
	CreateSolidBrush(RGB(255, 216, 0)),
	0,
	DoingNothing,
	DoingNothing2
};

Item IronSword = {
	IRON_SWORD,
	IronSvrd,
	_T("�������� ���"),
	{
		_T("������� ����� ������� �"),
		_T("������������ ���� ���"),
		_T("������� � ������������"),
		_T("��������� ��������. �������"),
		_T("�� ����� ���� ��� ������ �"),
		_T("�� ������� ��������"),
		_T("������������."),
		_T("������� E, ����� �����������"),
		_T("��� ����� �������")
	},
	CreateSolidBrush(RGB(64, 64, 64)),
	0,
	DoingEqip,
	DoingReneme,
	true,
	7//������������ ���� ������� ������ - ���������� - 2;
};

Item LeatherJacket = {
	LEATHER_JACKET,
	LJacket,
	_T("������� ������"),
	{
		_T("���������� �������� ������"),
		_T("�������� �����-�� ����"),
		_T("�������������� ��������"),
		_T("��������. � ����� ������"),
		_T("��� ������� �� �����"),
		_T("���������, ���� ��������"),
		_T("������� ��� ��������� ������"),
		_T("��������."),
		_T("������� � ����� �����"),
		_T("��� ����� �������")
	},
	CreateSolidBrush(RGB(139,69,19)),
	0,
	DoingEqip,
	DoingReneme,
	false,
	0,
	true,
	3
};

Item Key = { 
	KEY,
	IronKey,
	_T("����"),
	{
		
	},
	CreateSolidBrush(RGB(0, 255, 25)),
	0,
	DoingNothing,
	DoingNothing2,
};

Item Poushen_1 = {
	POUSHEN_1,
	Poush,
	_T("����� �������"),
	{
		_T("��� �� �� ������ ����"),
		_T("�������, �� ���� �� �����"),
		_T("��������� ���. ���� ����"),
		_T("������� ���������� �����,"),
		_T("�� ��������, ��� ����,"),
		_T("��������, ����� ��������"),
		_T("�������� ������������."),
		_T("������: ����� 10% ��"),
		_T("������������� ��������."),
		_T("�������� � ����� ������"),
		_T("�����.")
	},
	NULL,
	-1,
	DoingDrinkPoushen,
	DoingThrowPoushen
};

Item Poushen_2 = {
	POUSHEN_2,
	Poush,
	_T("����� �����"),
	{
		_T("��������� ����� ��������"),
		_T("����� �� ��� ����������."),
		_T("��� ��������� �� ����"),
		_T("������ ��������� ��������"),
		_T("���������� �. ����� ��"),
		_T("�������� ���� ��������,"),
		_T("���� ����������� ����� ����"),
		_T("����������."),
		_T("������: ������� 25 �����"),
		_T("�������� � ����� ������"),
		_T("�����.")

	},
	NULL,
	-1,
	DoingDrinkPoushen,
	DoingThrowPoushen
};

Item Poushen_3 = {
	POUSHEN_3,
	Poush,
	_T("����� �������"),
	{
		_T("���� ��������� �������"),
		_T("�� ����� ������ �������"),
		_T("� ����������������:"),
		_T("����� ����� ���������"),
		_T("������, ����� �������"),
		_T("�����. � ����������"),
		_T("�� ���������� ����"),
		

	},
	NULL,
	-1,
	DoingDrinkPoushen,
	DoingThrowPoushen
};

Item Poushen_4 = {
	POUSHEN_4,
	Poush,
	_T("����� �����������"),
	{
		_T("�������� ������� ��������"),
		_T("�� ��� �����. �� ���������"),
		_T("��������� �����������"),
		_T("���� � �������������"),
		_T("������:"),
		_T("10 ����� ��������������"),
		_T("�� 2 ��������/���"),


	},
	NULL,
	-1,
	DoingDrinkPoushen,
	DoingThrowPoushen
};

Item Poushen_5 = {
	POUSHEN_5,
	Poush,
	_T("����� ���������"),
	{
		_T("������������ ������� �"),
		_T("������ �������. ��������"),
		_T("���������� �������� ����"),
		_T("��������."),
		_T("������:"),
		_T("���������� ��� �������"),
		_T("�� 4 ����(��� �� ���������)")


	},
	NULL,
	-1,
	DoingDrinkPoushen,
	DoingThrowPoushen
};

Item Identigraf = {
	IDENTIGRAF,
	Ident,
	_T("������ ���������"),
	{_T("������� � ��������������"),
	_T("���� ����� �� ���� �����"),
	_T("����������. �����, ��� �����"),
	_T("������ ��� ����� ������"),
	_T("��� ������� ����-����."),
	_T("� ��������� ����� �������"),
	_T("����� �����������."),
	_T("������� � ��� ����������."),
	_T("������:"),
	_T("������� ������� � ���������"),},
	CreateSolidBrush(RGB(247, 245, 163)),
	0,
	DoingIdentifi,
	DoingNothing2
};

Item UnknownPoushen = {
	NULL_ITEM,
	Poush,
	_T("����������� �����"),
	{
		_T("���������� ������� ��"),
		_T("��������, ������� ���������."),
		_T("������� �� �������� �����"),
		_T("������� �� �������, ��� ���"),
		_T("����� �� �����, ��� �����"),
		_T("���� ������ ���. �������"),
		_T("���� ����������� � ������."),
		_T("�������� � ����� ������"),
		_T("�����.")
	}
};
#pragma endregion


Item AllItems[] = { NullItem,GoldenCoin,IronSword,LeatherJacket,Key,Poushen_1,Poushen_2,Poushen_3,Poushen_4,Poushen_5,Identigraf };


#pragma region �������
//���������� ������� �� �� ��������
void EffectNothing(void* player, int time, int pover=-1, int type =0);
void EffectHunger(void* player, int time, int pover = -1, int type = 0);
void EffectSaturation(void* player, int time, int pover = -1, int type = 0);
void EffectExhaustion(void* player, int time, int pover = -1, int type = 0);
void EffectRegeneration(void* player, int time, int pover = -1, int type = 0);
void EffectUpgrade(void* player, int time, int pover = -1, int type = 0);

Effect NullEffect;


Effect Saturation = { 
	SATURATUON,
	60,
	_T("���������"),
	{
		_T("�� ���� � ������ �"),
		_T("����������� �������."),
		_T("������:"),
		_T("+1 � �����,��������������"),
		_T("1 �������� �� ���.")

	},
	EffectSaturation
};

Effect Hunder = { 
	HUNGER,
	50,
	_T("�����"),
	{
		_T("�� ����������� ����� �����."),
		_T("��� �� ������ ��� ����������"),
		_T("������������, �� ����� "),
		_T("���������� ������ ����"),
		_T(" � ��������� �����."),
		_T("������:"),
		_T("�������"),
	},
	EffectHunger
};

Effect Exhaustion = {
	EXHAUSTION,
	100,
	_T("���������"),
	{
		_T("��� ������ ���������� ������"),
		_T("� ����� ��������� ��"),
		_T("�� ������ ���� ������"),
		_T("��� �����������."),
		_T("������: "),
		_T("�� ��������� 1 �������"),
		_T("����� �� ���."),
	},
	EffectExhaustion
};

Effect Regeneration = {
	REGENERATION,
	10,
	_T("�����������"),
	{
		_T("�������� ��������������"),
		_T("��������. ����� �������"),
		_T("�� ����� ��������� ���."),
		_T("������:"),
		_T("�������������� 2 ��������"),
		_T("�� ���."),
		
	},
	EffectRegeneration
};

Effect Upgreiging = {
	UPGRADE,
	1,
	_T("���������"),
	{
		_T("��������� ���� �������� "),
		_T(" ����������� ������. ������"),
		_T("��� �� ����������, ��� � ��"),
		_T("���������� �������."),
		_T("������:"),
		_T("�� 2 ���� ����������"),
		_T("��� �������."),

	},
	EffectUpgrade
};
#pragma endregion

#pragma region ������ ������ ��������
void EffectNothing(void * player, int time, int pover, int type) {
	if (type = 0) {
		Player *Pl = static_cast<Player*>(player);
	}
	else {
		Enemy *En = static_cast<Enemy*>(player);
	}
}

void EffectHunger(void * player, int time, int pover, int type) {
	if (type == 0) {
		Player *Pl = static_cast<Player*>(player);
		if (time == 0) {
			Pl->AddEffect(Exhaustion);
		}
	}
	else {
		Enemy *En = static_cast<Enemy*>(player);
		if (time == 0) {
			En->AddEffect(Exhaustion);
		}
	}
}


void EffectSaturation(void * player, int time, int pover, int type) {

	if (type == 0) {
		Player *Pl = static_cast<Player*>(player);
		if (time == 60 - 1) {
			Pl->MyArmor += 1;
		}
		else if (time == 0) {
			Pl->MyArmor -= 1;
			Pl->AddEffect(Hunder);
		}
		Pl->GetDamage(-1, false);
	}
	else {
		Enemy *En = static_cast<Enemy*>(player);
		if (time == 60 - 1) {
			//En->MyArmor += 1;
			En->AddEffect(Regeneration, 60, 1);
		}


		else if (time == 0) {
			//En->MyArmor -= 1;
			En->AddEffect(Hunder);
		}
	}
}

void EffectExhaustion(void * player, int time, int pover, int type) {
	if (type == 0) {
		Player *Pl = static_cast<Player*>(player);
		Pl->GetDamage(1, false);
		if (time == 0) {
			Pl->AddEffect(Exhaustion);
		}
	}
	else {
		Enemy *En = static_cast<Enemy*>(player);
		En->GetDamage(1);
		if (time == 0) {
			En->AddEffect(Exhaustion);
		}
	}
}

void EffectRegeneration(void * player, int time, int pover, int type) {
	if (type == 0) {
		Player *Pl = static_cast<Player*>(player);
		Pl->GetDamage((pover == -1) ? -2 : -pover, false);
	}
	else {
		Enemy *En = static_cast<Enemy*>(player);
		En->GetDamage((pover == -1) ? -2 : -pover);
	}
}

void EffectUpgrade(void * player, int time, int pover, int type) {
	if (type == 0) {
		Player *Pl = static_cast<Player*>(player);
		for (int i = 0; i < EFFECT_BAR_SIZE; i++)
		{
			if (Pl->EffectBar[i].ID != NULL_EFFECT && Pl->EffectBar[i].ID != UPGRADE) {
				Pl->EffectBar[i].Time += 3;
			}
		}
	}
	else {
		Enemy *En = static_cast<Enemy*>(player);
		for (int i = 0; i < EFFECT_BAR_SIZE; i++)
		{
			if (En->EffectBar[i].ID != NULL_EFFECT && En->EffectBar[i].ID != UPGRADE) {
				En->EffectBar[i].Time += 3;
			}
		}
	}
}
#pragma endregion

Effect ALLEffects[] = {NullEffect, Saturation,Hunder,Exhaustion,Regeneration,Upgreiging };


void DoingNothing(void* player, void* thItem) {

}

void DoingNothing2(void* player, void* thItem,void*enemy) {

}

void DoingEqip(void* player, void* thItem) {
	Player* PL = static_cast<Player*>(player);
	Item* thisI = static_cast<Item*>(thItem);
	int EqipType = (thisI->IsWeapon) ? 0 : ((thisI->IsArmor) ? 1 : 2);
	if (PL->Selectj != -1) {
		Item last = AllItems[0];

		if (PL->MyAmmunition[EqipType].ID != NULL_ITEM) {
			last = PL->MyAmmunition[EqipType];
		}
		if (EqipType == 0) {
			PL->MyDamage -= last.Damage - thisI->Damage;//��� ���� - ������ ���� + ����� ����
		}
		else if (EqipType == 1) {
			PL->MyArmor -= last.Armor - thisI->Armor;//��� ����� - ������ ����� + ����� �����
		}
		PL->MyAmmunition[EqipType] = *thisI;
		PL->MyInventory[PL->SelectI][PL->Selectj] = last;
	}
	else {
		for (int i = 0; i < INVENTORY_SIZE; i++)
		{
			for (int j = 0; j < INVENTORY_SIZE; j++)
			{
				if (PL->MyInventory[i][j].ID == NULL_ITEM) {
					PL->MyInventory[i][j] = *thisI;
					PL->MyAmmunition[PL->SelectI] = AllItems[0];
					if (EqipType == 0) {
						PL->MyDamage -= thisI->Damage;
					}
					else if (EqipType == 1) {
						PL->MyArmor -= thisI->Armor;
					}
				}
			}
		}
	}
}

void DoingReneme(void* player, void * thItem, void* enemy) {
	Player* PL = static_cast<Player*>(player);
	Item* thisI = static_cast<Item*>(thItem);
	char NewName[40];
	sprintf_s(NewName, "%s %s", Ajetivs[GetRandomInt(0, 12)], Noun[GetRandomInt(0, 12)]);
	TCHAR res[40] = {};
	convertToTCHAR(NewName, res);
	for (int i = 0; i < 40; i++)
	{
		
		thisI->name[i] = res[i];
	}
}

void DoingDrinkPoushen(void* player, void* thItem) {
	Player* PL = static_cast<Player*>(player);
	Item* thisI = static_cast<Item*>(thItem);
	if (PL->Combi==-1&&PL->Combj==-1) {
		PL->MyInventory[PL->SelectI][PL->Selectj].count--;
		AllItems[thisI->ID - NULL_ITEM].count = 0;

		switch (thisI->ID)
		{
		case POUSHEN_1:
			PL->GetDamage(-PL->MaxHp / 10, false);
			break;
		case POUSHEN_2:
			PL->GetDamage(25, false);
			break;
		case POUSHEN_3:
			if (PL->RemoveEffect(EXHAUSTION)) {
				PL->AddEffect(ALLEffects[HUNGER - NULL_EFFECT]);
			}
			else if (PL->RemoveEffect(HUNGER)) {
				PL->AddEffect(ALLEffects[SATURATUON - NULL_EFFECT]);
			}
			else {
				PL->AddEffect(ALLEffects[SATURATUON - NULL_EFFECT]);
			}
			break;
		case POUSHEN_4:
			PL->AddEffect(ALLEffects[REGENERATION - NULL_EFFECT]);
			break;
		case POUSHEN_5:
			PL->AddEffect(ALLEffects[UPGRADE - NULL_EFFECT]);
			break;
		}

		if (PL->MyInventory[PL->SelectI][PL->Selectj].count <= 0) {
			PL->MyInventory[PL->SelectI][PL->Selectj] = AllItems[0];
		}
	}
	else {
		AllItems[thisI->ID - NULL_ITEM].count = 0;
		PL->MyInventory[PL->Combi][PL->Combj].count--;
		if (PL->MyInventory[PL->Combi][PL->Combj].count <= 0) {
			PL->MyInventory[PL->Combi][PL->Combj] = AllItems[0];
		}
		PL->MyInventory[PL->Combi][PL->Combj].MyBrush = AllItems[PL->MyInventory[PL->Combi][PL->Combj].ID - NULL_ITEM].MyBrush;
		PL->Combi = -1;
		PL->Combj = -1;
	}
	
}

void DoingThrowPoushen(void* player, void * thItem, void* enemy) {
	Player* PL = static_cast<Player*>(player);
	Item* thisI = static_cast<Item*>(thItem);
	Enemy* En = static_cast<Enemy*>(enemy);

	PL->MyInventory[PL->Thi][PL->Thj].count--;
	AllItems[thisI->ID - NULL_ITEM].count = 0;

	switch (thisI->ID)
	{
	case POUSHEN_1:
		En->GetDamage(-PL->MaxHp / 10);
		break;
	case POUSHEN_2:
		En->GetDamage(25);
		break;
	case POUSHEN_3:
		if (En->RemoveEffect(EXHAUSTION)) {
			En->AddEffect(ALLEffects[HUNGER - NULL_EFFECT]);
		}
		else if (En->RemoveEffect(HUNGER)) {
			En->AddEffect(ALLEffects[SATURATUON - NULL_EFFECT]);
		}
		else { En->AddEffect(ALLEffects[SATURATUON - NULL_EFFECT]); }
		break;
	case POUSHEN_4:
		En->AddEffect(ALLEffects[REGENERATION - NULL_EFFECT]);
		break;
	case POUSHEN_5:
		En->AddEffect(ALLEffects[UPGRADE - NULL_EFFECT]);
		break;
	}

	if (PL->MyInventory[PL->Thi][PL->Thj].count <= 0) {
		PL->MyInventory[PL->Thi][PL->Thj] = AllItems[0];
		PL->Thi = -1;
		PL->Thj = -1;
	}
}

void DoingIdentifi(void* player, void* thItem) {
	Player* PL = static_cast<Player*>(player);
	Item* thisI = static_cast<Item*>(thItem);
	if (PL->Combi == -1 && PL->Combj == -1) {
		for (int i = 0; i < INVENTORY_SIZE; i++)
		{
			for (int j = 0; j < INVENTORY_SIZE; j++)
			{
				if (PL->MyInventory[i][j].ID == thisI->ID &&PL->MyInventory[i][j].count == thisI->count) {
					PL->Combi = i;
					PL->Combj = j;
					PL->MyInventory[i][j].MyBrush = (HBRUSH)GetStockObject(NULL_BRUSH);
				}
			}
		}
	}
	else {
		PL->MyInventory[PL->Combi][PL->Combj].MyBrush = AllItems[PL->MyInventory[PL->Combi][PL->Combj].ID - NULL_ITEM].MyBrush;
		PL->Combi = -1;
		PL->Combj = -1;
	}
	

}


