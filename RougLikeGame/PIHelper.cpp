#include "stdafx.h"
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <string>
#include "PIHelper.h"
#include <time.h>

using namespace std;

string StrSrlitOfIndex(string str, char s, int ind) {
	string tmp = "";
	int count = 0;

	for (int i = 0; i < str.length(); i++)
	{
		if (str[i] != s) {
			tmp += str[i];
		}
		else {
			if (count == ind) {
				return tmp;

			}
			else {
				tmp = "";
			}
			count++;
		}

	}
	if (count == ind) {
		return tmp;
	}
	return "&|&";

}

int StrSplit(string str, char sp, string* Output) {
	int counter = 0;
	string tmp = StrSrlitOfIndex(str, sp, counter);
	while (tmp != "&|&")
	{
		Output[counter] = tmp;
		counter += 1;
		tmp = StrSrlitOfIndex(str, sp, counter);

	}
	return counter;
}

string StrJoin(string* Mass, string joiner, int MassLen) {
	string res = "";
	for (int i = 0; i < MassLen; i++)
	{
		if (i + 1 == MassLen) {
			res += Mass[i];
			break;
		}
		res += Mass[i] + joiner;
	}
	return res;
}

int GetRandomInt(int minv, int maxv) {

	int Dist = maxv - minv;
	int res = rand() % (Dist + 1) + minv;
	return res;
}

void InitRandom(int key) {
	if (key == -1) {
		srand(time(nullptr));
		srand(GetRandomInt(-10000, 10000) + time(nullptr) * GetRandomInt(1, 100));
	}
	else {
		srand(key);
	}
}

string FileReabAllLines(string path) {
	string res;
	ifstream in(path);
	string tmp;
	while (getline(in, tmp)) {

		res += tmp + "\n";
	}
	return res;
}

void FIleWrileAllLines(string path, string lines) {
	ofstream out;
	out.open(path);
	out << lines;
}

void convertToTCHAR(char* a, TCHAR* r) {
	for (int i = 0; i < strlen(a); i++)
	{
		if (*(a + i) >= 0) {
			*(r + i) = *(a + i);
		}
		else {
			if (*(a + i) >= '�'&&*(a + i) <= '�') {
				*(r + i) = *(a + i) - '�' + _T('�');
			}
			else if (*(a + i) == '�') {
				*(r + i) = 0x401;
			}
			else if (*(a + i) == '�') {
				*(r + i) = 0x451;
			}
		}

	}
	*(r + strlen(a)) = '\0';
}

void convertFromTCHAR(TCHAR* a, char* r) {
	int i;
	for (i = 0; *(a + i) != 0; i++)
	{
		if (*(a + i) <= '~') {
			*(r + i) = *(a + i);
		}
		else {
			if (*(a + i) >= _T('�') && *(a + i) <= _T('�')) {
				*(r + i) = *(a + i) - _T('�') + '�';
			}
			else if (*(a + i) == 0x401) {
				*(r + i) = '�';
			}
			else if (*(a + i) == 0x451) {
				*(r + i) = '�';
			}
		}
	}
	*(r + i) = '\0';
}