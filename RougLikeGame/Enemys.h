#pragma once
#include "stdafx.h"
#include "RougLikeGame.h"
#include <stdio.h>
#include <ctime>
#include "MapGenerator.h"
#include"PIHelper.h"
#include "Entities.h"


struct Enemy
{
	int x;
	int y;
	int Type = 1;
	int LastBlock = 0;
	int Path[100][2];
	int OnPath = -1;
	int HP = 30;
	int maxHp = 30;
	int Die = 0;
	int Damage = 10;
	Effect EffectBar[50];
	void GetDamage(int damage);
	double GetProcent();
	void AddEffect(Effect eff, int time = -1, int pover = -1);
	bool RemoveEffect(int id);
};

extern int EnemyCoord[(MAP_LENTH / CELL_SIZE) * (MAP_LENTH / CELL_SIZE)][2];
extern int EnemyCount;

extern Enemy AllEnemys[(MAP_LENTH / CELL_SIZE) * (MAP_LENTH / CELL_SIZE)];

void CreateAllEnemys();
void MoveAllEnemy(void* Pl);
Enemy * GetEnemyFromCoord(int x, int y);